﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

    public static int MaxBitwiseValue(int n, int k){
        int maxValue =0;
        for(int i = 1; i <= n; i++){
            for(int j = 1; j < i; j++){
                int and = i & j;
                if(maxValue < and && and < k){
                    maxValue = and;
                    if(maxValue == k - 1){
                        return maxValue;
                    }
                }
            }
        }
        return maxValue;
    }

    static void Main(string[] args) {
        int t = Convert.ToInt32(Console.ReadLine());

        for (int tItr = 0; tItr < t; tItr++) {
            string[] nk = Console.ReadLine().Split(' ');

            int n = Convert.ToInt32(nk[0]);

            int k = Convert.ToInt32(nk[1]);

            Console.WriteLine(MaxBitwiseValue(n, k));
        }
    }
}
